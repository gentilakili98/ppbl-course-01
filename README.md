# Plutus Project Based Learning
## Course #1 | Gimbalabs | February - April 2022

To participate in help and discussions about this project, you must be enrolled in Plutus Project-Based Learning course at Gimbalabs. If you are enrolled and have questions, please access the discussion boards on Canvas.

## Course Outline

### Project #1: Prepare Your Plutus Environment + Compile a Plutus Script (Week 1)

### Project #2: Mint a Token (Weeks 2-3)

### Project #3: Build a Game (Weeks 4-6)

### Project #4: Capstone Project (Weeks 7-10)
