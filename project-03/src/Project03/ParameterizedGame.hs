{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Project03.ParameterizedGame where

import           Data.Map            as Map
import           Data.Text           (Text)
import           Data.Void           (Void)
import           Plutus.Contract
import           PlutusTx            (Data (..))
import qualified PlutusTx
import qualified PlutusTx.Builtins   as Builtins
import           PlutusTx.Prelude    hiding (Semigroup(..), unless)
import           Ledger              hiding (singleton)
import           Ledger.Constraints  as Constraints
import qualified Ledger.Typed.Scripts as Scripts
import           Ledger.Ada          as Ada
import           Prelude             (IO, Semigroup (..), Show (..), String)
import           Text.Printf         (printf)

{-# OPTIONS_GHC -fno-warn-unused-imports #-}

data GameAction = Guess | EndGame
    deriving Show

PlutusTx.makeIsDataIndexed ''GameAction [('Guess, 0), ('EndGame, 1)]
PlutusTx.makeLift ''GameAction

data GameParam = GameParam
    { gameHost :: !PaymentPubKeyHash
    , answer   :: !Integer
    }

PlutusTx.makeLift ''GameParam

-- start with a bad implementation, then live code this week

-- hi i am thinking like how to write reedemer with more data say integer to write a transaction in frontend part

-- Datum: some identifier that says "These UTxOs belong to THIS game" + instructions on how to retreive, how to validate/redeem
-- Another game instance would have another Datum
-- What functions could I use to look at "all of the UTxOs with THIS datum hash"?

-- Datum "somehow should be public" -- agree / disagree?
-- Blockfrost, for example provides the endpoint:
-- https://docs.blockfrost.io/#tag/Cardano-Scripts/paths/~1scripts~1{script_hash}~1redeemers/get

-- Can you write a Redeemer so that the "player" can say "I want Guess #" rather than simply "I want to Guess"?

-- What would some sort of "Start Game" endpoint need to do?

-- How to get pkh

-- The code below is a "bucket" and it defines some rules
-- We can reuse the rules
-- ...to create unique instance of "bucket"

-- Is this game fair?
-- Is there a way to prove that it's fair?
-- Time limit? Does the host have to do something, or wait for something before ending a game + collecting funds?

-- Our game will not be fair if the host knows their answer. They can go win their own game any time.

{-# INLINABLE mkValidator #-}
mkValidator :: GameParam -> Integer -> GameAction -> ScriptContext -> Bool
mkValidator gp num action ctx =
    case action of
        Guess    -> traceIfFalse "Your guess is wrong!" checkGuess &&
                    traceIfFalse "You need a game play token" checkHasPlayToken &&
                    traceIfFalse "You must send at least 5 tAda to the contract" checkIsGambling
        EndGame  -> traceIfFalse "You are not the host of this Guess" checkIsHost
    where
        info :: TxInfo
        info = scriptContextTxInfo ctx

        checkGuess :: Bool
        checkGuess = True

        checkHasPlayToken :: Bool
        checkHasPlayToken = True

        checkIsGambling :: Bool
        checkIsGambling = True

        checkIsHost :: Bool
        checkIsHost = True

        checkIsPastDeadline :: Bool
        checkIsPastDeadline = True

data GuessingGame
instance Scripts.ValidatorTypes GuessingGame where
    type instance DatumType GuessingGame = Integer
    type instance RedeemerType GuessingGame = GameAction

typedValidator :: GameParam -> Scripts.TypedValidator GuessingGame
typedValidator gp = Scripts.mkTypedValidator @GuessingGame
    ($$(PlutusTx.compile [|| mkValidator ||]) `PlutusTx.applyCode` PlutusTx.liftCode gp)
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @Integer @GameAction

validator :: GameParam -> Validator
validator = Scripts.validatorScript . typedValidator