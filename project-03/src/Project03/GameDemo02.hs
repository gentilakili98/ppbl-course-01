{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Project03.GameDemo02 where

import           Data.Map            as Map
import           Data.Text           (Text)
import           Data.Void           (Void)
import           Plutus.Contract
import           PlutusTx            (Data (..))
import qualified PlutusTx
import qualified PlutusTx.Builtins   as Builtins
import           PlutusTx.Prelude    hiding (Semigroup(..), unless)
import           Ledger              hiding (singleton)
import           Ledger.Constraints  as Constraints
import qualified Ledger.Typed.Scripts as Scripts
import           Ledger.Ada          as Ada
import           Prelude             (IO, Semigroup (..), Show (..), String)
import           Text.Printf         (printf)

{-# OPTIONS_GHC -fno-warn-unused-imports #-}

-- 3.1.5
-- Video lecture: "What can a UTxO hold?"
-- Context can show that a UTxO holds tokens. Let's pick up where we left off in Week 3 by revisiting TxInfo
    -- With ScriptContext, our Validator can:
        -- check that a certain player is playing
        -- check that a transaction is happening within certain time boundaries
        -- check whether a participant holds a certain token (We will need to build transactions that can prove this)

-- A UTxO can hold Datum (see ccli). Let's take a first look at how Datum works in a Smart Contract
    -- In a guessing game, Datum would hold the "answer"


-- Redeemer can pass information to the Validator: here is how it's often used --> ACTIONS in the context of a Game
    -- In a guessing game, we might have a "Host" and a "Guesser". We will need ways for both of them to interact withe the contract.
    -- The Redeemer is where a participant can identify themselves to the contract.
    -- Particpants can state the Action they'd like to take
        -- Hosts can start or end a contest
        -- Guessers can try to guess a number (essentially hacking our contract...like we saw in 3.1.2)

data GameAction = Guess | EndGame
    deriving Show

PlutusTx.makeIsDataIndexed ''GameAction [('Guess, 0), ('EndGame, 1)]
PlutusTx.makeLift ''GameAction

data GameDatum = GameDatum
    { gameHost :: PaymentPubKeyHash
    , answer   :: Integer
    }

PlutusTx.unstableMakeIsData ''GameDatum

-- start with a bad implementation, then live code this week

{-# INLINABLE mkValidator #-}
mkValidator :: GameDatum -> GameAction -> ScriptContext -> Bool
mkValidator gdata action ctx =
    case action of
        Guess    -> traceIfFalse "Your guess is wrong!" checkGuess &&
                    traceIfFalse "You need a game play token" checkHasPlayToken &&
                    traceIfFalse "You must send at least 5 tAda to the contract" checkIsGambling
        EndGame  -> traceIfFalse "You are not the host of this Guess" checkIsHost
    where
        info :: TxInfo
        info = scriptContextTxInfo ctx

        checkGuess :: Bool
        checkGuess = True
        -- now that the answer is in the datum, what we really want is for the guesser to supply their
        -- guess another way...we'll need to extend our redeemer

        checkHasPlayToken :: Bool
        checkHasPlayToken = True
        -- one way to let someone play is with a token, but we're not going to implement it quite yet.
        -- first, let's just see if the player is ready to gamble
        -- maybe live coding on Friday? or just Week 5...

        checkIsGambling :: Bool
        checkIsGambling = True
        -- we just want to know if the player is submitting money to the contract
        -- but there's a problem. the contract will only allow them to play if they're going to win in the first place
        -- this game can only lose us money
        -- so we have to think a bit more about it: join us for live coding this week! (Maybe Thursday)
        -- Here is how to extend our Redeemer

        checkIsHost :: Bool
        checkIsHost = txSignedBy info $ unPaymentPubKeyHash $ gameHost gdata
        -- How does a contract address know where a UTxO came from??
        -- We can store it in Datum.
        -- This will require a special new data type...
        -- ...and eventually we will find that a parameterized Validator
        -- so: (a) Why? (b) How would we compile this?
        -- (c) What does this mean for addresses? (d) How will we create those addresses?

data GuessingGame
instance Scripts.ValidatorTypes GuessingGame where
    type instance DatumType GuessingGame = GameDatum
    type instance RedeemerType GuessingGame = GameAction

typedValidator :: Scripts.TypedValidator GuessingGame
typedValidator = Scripts.mkTypedValidator @GuessingGame
    $$(PlutusTx.compile [|| mkValidator ||])
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @GameDatum @GameAction

validator :: Validator
validator = Scripts.validatorScript typedValidator