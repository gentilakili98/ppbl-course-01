## Project 3, Section 3.1.5

Are you starting to know the drill?

### Compile what we've got

```
Prelude Project03.PlutusGameCompiler> writeGameDemoScript
```

### Create a Contract Address

```
cd /project-03/output
cardano-cli address build --payment-script-file game-demo-03.plutus --testnet-magic 1097911063 --out-file game-demo-03.addr
```
Note: we're calling this address `game-demo-v1` because we're going to change our Plutus code next. Remember that addresses are deterministic. If you're whether you've used the same code, you can check addresss. For example, `game-demo-v1.addr` should be:

```
addr_test1wzax9v29gq45veuta6x44w4kytdftcucn23s5l7rp8z95jg8hkkfs
```

This is the address of a contract that expects a Redeemer equal to "Guess" or "EndGame" and a Datum equal to 314159. But does it really work? Let's see what we're able to do.

### Build a valid transaction to lock

Get the datum-hash of 314159
```
cardano-cli transaction hash-script-data --script-data-value 314159
> 47b1f61d406a474f68b9cc894582babab2e0a3dfc505fa338a9285e7bf6f09b2
```

Get a wrong datum-hash: 100, for example (could be anything except 314159)
```
cardano-cli transaction hash-script-data --script-data-value 100
> 67882f9c671cb45fc6990a2d14a20b30bfce29ad99a401c283a100662e6600fb
```

#### Set Variables

```
SENDER=
SENDERKEY=
CONTRACTADDR=addr_test1wzax9v29gq45veuta6x44w4kytdftcucn23s5l7rp8z95jg8hkkfs
TXIN=
DATUMHASH="47b1f61d406a474f68b9cc894582babab2e0a3dfc505fa338a9285e7bf6f09b2"
TESTNET="--testnet-magic 1097911063"
COLLATERAL=
```

#### Lock:
```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN \
--tx-out $CONTRACTADDR+3000000 \
--tx-out-datum-hash $DATUMHASH \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx-v1.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx-v1.raw \
--out-file tx-v1.signed

cardano-cli transaction submit \
--tx-file tx-v1.signed \
--testnet-magic 1097911063

```

### Build a valid transaction to unlock

How to get the Redeemer right?
Are we allowed to unlock these contracts?
Remember: TXIN comes from Contract this time

Set Variables
```
CONTRACTADDR="addr_test1wzax9v29gq45veuta6x44w4kytdftcucn23s5l7rp8z95jg8hkkfs"
TXINGOODDATUM="2200be367b32ee53bc6cfa1c6e43174899df52fc3128ee1fa8d18e69b5fa0379#1"
SCRIPTFILE="game-demo.plutus"
GUESS="guess.json"
TESTNET="--testnet-magic 1097911063"
COLLATERAL="6308cc82a08ec7a3c19ee0ca8e659f564dc39d5e37dcae3ba642c31d986e64fa#10"
TXINBADDATUM="ae9fc5f288aa7f348b6e5216452f99bbedac38676f1b8d2e66101f986551a8ef#1"
ENDGAME=endgame.json
BADREDEEMER= path to /ppbl-course-01/project-03/output/shouldnotwork.json
```

```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXINBADDATUM \
--tx-in-script-file $SCRIPTFILE \
--tx-in-datum-value 100 \
--tx-in-redeemer-file shouldnotwork.json \
--tx-in-collateral $COLLATERAL \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file unlock-v1.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock-v1.raw \
--out-file unlock-v1.signed

cardano-cli transaction submit \
--tx-file unlock-v1.signed \
--testnet-magic 1097911063
```

### What is going on here?
- can we unlock with wrong number but never unlock?
- that doesn't seem great...



## GameDemo02
Get pkh:
```
cardano-cli address key-hash --payment-verification-key-file payment.vkey > host02.pkh
```

Create Datum
```
datum02.json
```

Get Datum Hash
```
cardano-cli transaction hash-script-data --script-data-file datum02.json
```

Set Variables
```
SENDER=
SENDERKEY=
TXIN=
CONTRACTADDR=addr_test1wzh8y9ztx9k4nhyl92vnlq4g290rvk3w5fg3aytvw6xpj7qs7pyuk
DATUMHASH="c4c8523115f37a6523510618dcd4f7c22e5dd66aa47decadbb7fd1347e117000"
TESTNET="--testnet-magic 1097911063"
COLLATERAL=
```

LOCK:
```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN \
--tx-out $CONTRACTADDR+3000000 \
--tx-out-datum-hash $DATUMHASH \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx-game-02.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx-game-02.raw \
--out-file tx-game-02.signed

cardano-cli transaction submit \
--tx-file tx-game-02.signed \
--testnet-magic 1097911063
```

### UNLOCK
#### Set:
```
CONTRACTTXIN=8b90f54a3e751a5ca225d39f3466d89d6a5e5e73219e1a55c1c806b2ffc78d72#1
SCRIPTFILE=game-demo-03.plutus
DATUMFILE=datum02.json
REDEEMERFILE=guess.json
COLLATERAL
```
#### Build:

```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $CONTRACTTXIN \
--tx-in-script-file $SCRIPTFILE \
--tx-in-datum-file $DATUMFILE \
--tx-in-redeemer-file $REDEEMERFILE \
--tx-in-collateral $COLLATERAL \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file unlock-v1.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock-v1.raw \
--out-file unlock-v1.signed

cardano-cli transaction submit \
--tx-file unlock-v1.signed \
--testnet-magic 1097911063
```